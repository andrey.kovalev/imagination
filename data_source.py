import urllib.request as req
import pandas as pd
from IPython.display import clear_output

urls = {
    "weather": "https://ent.box.com/shared/static/f8ercbwzajmctnnrl1yrk0njxiafjdvi.csv",
    "trip": "https://ent.box.com/shared/static/z01cdoktzwfy2qnnnt6g30997xfikq94.csv",
    "station": "https://ent.box.com/shared/static/yl5iz6bw7bis6vgo8ooz04tc6fii7m04.csv",
    "status": "https://ent.box.com/shared/static/7gl6b28eucghvk1rt0r8np0yltkmob6q.csv",
    "status_change": "https://ent.box.com/shared/static/ckt6j7i4drujfsr2rn7qa0h4r468km9a.csv"
}

def loadData(filename, url):
    """Load CSV data into a DataFrame

    Keyword arguments:
    filename -- the CSV file to load the data from
    url -- in case the CSV file does not exist, it will be loaded from this URL and saved as 'filename'. Any consecutive call will use the local filename.

    Returns: the DataFrame with data
    """
    path = "data/" + filename;
    try:
        return pd.read_csv(path);
    except FileNotFoundError:
        print("Fetching %s from the net..." % (filename));
        req.urlretrieve(url, filename=path, reporthook=_download_hook);
        print("Done.");
        return pd.read_csv(path);

def loadDataset(datasetName):
    """Load a dataset into a DataFrame by name
    """
    if datasetName not in urls :
        raise ValueError('Unknown dataset: ' + datasetName);
    return loadData(datasetName+".csv", urls[datasetName]);

def _download_hook(chunkNum, maxChunkSize, totalSize):
    downloaded = chunkNum * maxChunkSize;
    percents = downloaded * 100 / totalSize;
    percents = min(percents, 100);
    clear_output(wait=True);
    print("Downloaded %.2f%%" % (percents));
    # print("chunkNum=%s, maxChunkSize=%s, totalSize=%s" % (chunkNum, maxChunkSize, totalSize))

print('data_source.py loaded')
