# Imagination challenge

See: [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/andrey.kovalev%2Fimagination/master?filepath=Intro.ipynb)

All notebooks assume that the data from here: https://www.kaggle.com/benhamner/sf-bay-area-bike-share is present in the data subfolder (extracted).
